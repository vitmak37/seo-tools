// ==UserScript==
// @name        toolbox dev
// @namespace   vitmak38
// @author      toolbox dev
// @homepageURL //vm-std.ru/scripts/toolbox.js
// @updateURL   https://bitbucket.org/vitmak37/seo-tools/src/master/dev/install_toolbox_dev.js
// @downloadURL https://bitbucket.org/vitmak37/seo-tools/src/master/dev/install_toolbox_dev.js
// @description toolbox dev
// @version     0.102
// @grant       GM_log
// @grant       GM_getResourceText
// @grant       GM_addStyle
// @grant       GM_xmlhttpRequest
// @grant       GM_getResourceURL
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       AWE_engineStatus
// @noframes
// @include     http://*.*/*
// @include     https://*.*/*
// ==/UserScript==



(() => {
    const XHR = ('onload' in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    const xhr = new XHR();

    xhr.open('GET', window.location, true);
    let charr = 'text/plain; charset='+document.characterSet;
    //xhr.overrideMimeType('text/plain; charset=utf-8');
    xhr.overrideMimeType(charr);

    xhr.send();
    xhr.onload = () => {
        let code = xhr.responseText;

        //code = new TextDecoder("windows-1252").decode(codde);

        //comment1
        const parser = new DOMParser();

        if (location.hostname === 'job.goldenstudio.ru')
        {
            console.log('Панель отключена на этом сайте');
            code = '';
        }
        else{
            code = parser.parseFromString(code, 'text/html');
            if (!code){console.log('Не удалось получить код страницы.');}
        }
        //code = parser.parseFromString(code, 'text/html');
        const script = code.querySelectorAll('script');
        function filterNone() {
            return NodeFilter.FILTER_ACCEPT;
        }
        const codeBody = code.getElementsByTagName('body')[0];
        if (!codeBody) {
            alert('Не удалось проверить из-за ошибок в HTML коде. Проверьте код страницы на валидность.');
            return;
        }



        // функция для удаления в строке двойных пробелов
        function dsr(str) {
            let returnStr = str;
            while (returnStr.indexOf('  ') + 1) {
                returnStr = returnStr.replace(/ {2}/g, ' ');
            }
            return returnStr;
        }

        let bodyText = codeBody.innerText.replace(/[\r\n\t]/gi, ' ');
        bodyText = dsr(bodyText);
        const comment = [];
        const iterator = document.createNodeIterator(code, NodeFilter.SHOW_COMMENT, filterNone, false);
        let curNode;
        while ((curNode = iterator.nextNode())) {
            comment.push(curNode.nodeValue);
        }
        let h = codeBody.querySelectorAll('h1, h2, h3, h4, h5, h6');
        if (!h) h = [];
        const hd = [];
        for (let i = 0; i < h.length; i += 1) {
            hd[i] = [];
            hd[i].head = Number(h[i].localName[1]);
            hd[i].text = h[i].textContent;
        }


        const tempHeaders = [];
        let hErr = false;
        for (let i = 0; i < hd.length; i += 1) {
            if (i === 0) {
                if (hd[0].head !== 1) {
                    hd[0].error = 'Первый заголовок не h1';
                    tempHeaders[hd[0].head] = true;
                    hErr = true;
                    continue;
                }
            }

            if (hd[i].head === 1 && tempHeaders[1]) {
                hd[i].error += 'Более одного заголовка H1. ';
            }

            if (hd[i].head === 1 && (tempHeaders[2] || tempHeaders[3] || tempHeaders[4] || tempHeaders[5] || tempHeaders[6])) {
                hd[i].error += 'Не первый заголовок в иерархии.';
            }

            if (hd[i].head !== 1 && !tempHeaders[hd[i].head - 1]) {
                hd[i].error += 'Перед заголовком не было заголовка уровнем выше. ';
            }

            if (hd[i - 1] && (hd[i].head - hd[i - 1].head > 1)) {
                hd[i].error += 'Нарушает иерархию заголовков. ';
            }

            if (hd[i].text.replace(/ |\s|&nbsp;/gi, '') === '') {
                hd[i].error += 'Пустой заголовок.';
            }
            tempHeaders[hd[i].head] = true;
            if (hd[i].error) {
                hd[i].error = hd[i].error.replace('undefined', '');
                hErr = true;
            }
        }

        let alertStr = '';
        let openLinks = '';
        let descr = code.querySelector('meta[name=description]') || document.querySelector('meta[name=description]');
        let keyw = code.querySelector('meta[name=keywords]') || document.querySelector('meta[name=keywords]');
        const meta = code.querySelectorAll('meta') || document.querySelectorAll('meta');
        const bcnt = codeBody.querySelectorAll('b');
        const strong = codeBody.querySelectorAll('strong');
        const em = codeBody.querySelectorAll('em');
        const links = codeBody.querySelectorAll('a');
        let externalLinks = '';
        let externalLinksCnt = 0;
        let internalLinks = '';
        let internalLinksCnt = 0;
        const img = codeBody.querySelectorAll('img');
        const titleAttr = codeBody.querySelectorAll('body [title]');
        let altTitle = '';
        let altCnt = 0;
        let altStrCnt = 0;
        let h16Str = '';
        let canonical = code.querySelector('link[rel=canonical]') || document.querySelector('head link[rel=canonical]');
        let rnext = code.querySelector('link[rel=next]') || document.querySelector('link[rel=next]');
        let rprev = code.querySelector('link[rel=prev]') || document.querySelector('link[rel=prev]');
        const title = code.querySelector('title') || document.querySelector('title');
        let codeScriptsDel = '';
        let speed;
        let spd;
        // на https://paybis.com/ не работает title и description
        // цикл виснет. Нужно не удалять src и href

        codeScriptsDel = code.getElementsByTagName('body')[0].cloneNode(true);






        javascript:(function(){
            var s=document.createElement('script');
            s.type='text/javascript';
            s.src='//vm-std.ru/scripts/metahilight.js';
            //s.setAttribute('charset','utf-8');
            document.getElementsByTagName('head')[0].appendChild(s)
        })();

        function codeClear(element) {
            const allTags = element.children;
            const allTagsLen = allTags.length;
            for (let i = 0; i < allTagsLen; i += 1) {
                if (allTags[i].nodeType !== 1) continue;
                if (['script', 'style', 'noscript'].indexOf(allTags[i].nodeName.toLowerCase()) !== -1) {
                    allTags[i].innerHTML = '';
                    continue;
                }
                const tagAttr = allTags[i].attributes;
                const attLent = tagAttr.length;
                if (attLent > 0) {
                    for (let a = 0; a < attLent; a++) {
                        // debugger;
                        if (!tagAttr[a]) break;
                        if (tagAttr[a].name === 'src' || tagAttr[a].name === 'href') continue;
                        allTags[i].removeAttribute(tagAttr[a].name);
                        a--;
                    }
                }
                if (allTags[i].childNodes.length > 0) {
                    allTags[i] = codeClear(allTags[i]);
                }
                element[i] = allTags[i];
                continue;
            }
            return element;
        }





        codeScriptsDel = codeClear(codeScriptsDel);

        for (let i = 0; i < meta.length; i += 1) {
            if (meta[i].name.toLowerCase() === 'description') descr = meta[i];
            if (meta[i].name.toLowerCase() === 'keywords') keyw = meta[i];
        }

        if (title) {
            // alertStr += `<p><b class="link_sim"  title="Скопировать title в буфер обмена">Title</b> <span ${(title.textContent.length < 30 || title.textContent.length > 150) ? "class='red'" : ''}>(${title.textContent.length}): </span>${title.textContent}</p>`;
        } else {
            //alertStr += '<p><b class="red">Title: отсутствует</b></p>';
        }

        if (descr) {
            descr = descr.content;
            if (descr) {
                //  descr = ' отсутствует ';
                //alertStr += `<p><b class="link_sim" title="Скопировать description в буфер обмена">Description</b> <span ${(descr.length < 50 || descr.length > 250) ? "class='red'" : ''}>(${descr.length}): </span>${descr}</p>`;
            } else {
                descr = ' отсутствует ';
                //alertStr += '<p><b class="red">Description: отсутствует</b></p>';
            }
        } else {
            descr = ' отсутствует ';
            //alertStr += '<p><b class="red">Description: отсутствует</b></p>';
        }


        if (keyw) {
            keyw = keyw.content;
            if (keyw) {
                //alertStr += `<p><b class="link_sim" title="Скопировать Keywords в буфер обмена">Keywords</b> <span>(${keyw.length}): </span>${keyw}</p>`;
            } else {
                //alertStr += '<p><b class="red">Keywords: отсутствует</b></p>';

                keyw = ' отсутствует';
            }
        } else {
            keyw = ' отсутствует';
        }

        /*if (keyw) {
          keyw = keyw.content;
          if (keyw) {
            //alertStr += `<p><b class="link_sim" title="Скопировать Keywords в буфер обмена">Keywords</b> <span>(${keyw.length}): </span>${keyw}</p>`;

          }
        }*/











        for (let i = 0; i < meta.length; i += 1) {
            if (meta[i].name.toLowerCase() === 'robots' || meta[i].name.toLowerCase() === 'yandex' || meta[i].name.toLowerCase() === 'googlebot') {
                alertStr += `<p><b>meta ${meta[i].name}:</b> ${(meta[i].content.indexOf('noindex') + 1 || meta[i].content.indexOf('nofollow') + 1) ? `<b class='red'>${meta[i].content}</b>` : meta[i].content}</p>`;
            }
            else
            {
                meta[i].name = 'robots';
                meta[i].content = 'нет содержимого  '

            }





        }

        if (canonical) {
            canonical = canonical.getAttribute('href');
            if (canonical) {
                //alertStr += `<p><b>Canonical:</b> ${(canonical === location.href) ? `<a href='${canonical}'>${canonical}</a>` : `<a href='${canonical}'><b class='red'>${canonical}</b></a>`}</p>`;
            }
        }

        if (rnext) {
            rnext = rnext.href;
            if (rnext) {
                alertStr += `<p><b>rel=next: </b><a href="${rnext}">${rnext}</a></p>`;
            }
        }

        if (rprev) {
            rprev = rprev.href;
            if (rprev) {
                alertStr += `<p><b>rel=prev: </b><a href="${rprev}">${rprev}</a></p>`;
            }
        }



        spd = 'https://developers.google.com/speed/pagespeed/insights/?url='+encodeURIComponent(location.href);
        let screen = '//mini.s-shot.ru/1280/400/png/?'+encodeURIComponent(location.href);


        /*

       $(document).ready(function () {
           var $forPhotosG = $('.alertStr_block'),
               $head_b = $('.alertStr_show');
           $('.alertStr_show').click(function () {
               $forPhotosG.finish();
               var vis = $('.alertStr_block').is(":visible"),
                   text = vis ? 'Показать' : 'Скрыть';
               $head_b.text(text);
               $forPhotosG.slideToggle("slow");


               return false;
           });
       });
      */

        alertStr += `<b class="alertStr_show"></b>`;
        alertStr += `<div class="alertStr_block">`;
        alertStr += `<div class="   ">`;
        alertStr += `<p><b class="title"></b></p>`;
        if (bcnt && bcnt.length > 0) {
            alertStr += `<p><b>Число b:</b> ${bcnt.length}</p>`;
        }




        if (strong && strong.length > 0) {
            alertStr += `<p><b>Число strong:</b> ${strong.length}</p>`;
        }

        if (em && em.length > 0) {
            alertStr += `<p><b>Число em:</b> ${em.length}</p>`;
        }

        if (comment && comment.length > 0) {
            let commentLen = 0;
            let maxCommentLen = 0;
            for (let i = 0; i < comment.length; i += 1) {
                commentLen += comment[i].length;
                if (comment[i].length - 7 > maxCommentLen) {
                    maxCommentLen = comment[i].length - 7;
                }
            }
            alertStr += `<p><b>HTML комментарии:</b> <span title="Количество HTML комментариев">${comment.length}</span> | <span title="Объем HTML комментариев (символов)">${commentLen}</span> | <span title="Длинна наибольшего комментария">${maxCommentLen}</span></p>`;
        }

        if (script && script.length > 0) {
            let scriptLen = 0;
            for (let i = 0; i < script.length; i += 1) {
                scriptLen += script[i].textContent.length;
            }
            alertStr += `<p><b>JS скрипты:</b> <span title="Количество внутренних JS"><br>${script.length}</span> | <span title="Объем JS кода (символов)">${scriptLen}</span></p>`;
        }

        const linksTempArr = [];
        let l = '';
        let lh = '';
        for (let i = 0; i < links.length; i += 1) {
            lh = links[i].href.split('#')[0];
            if (linksTempArr[lh]) continue;
            try {
                l = decodeURIComponent(lh);
            } catch (e) {
                l = lh;
            }
            if (location.hostname === links[i].hostname) {
                internalLinksCnt += 1;
                linksTempArr[lh] = true;
                internalLinks += `<li><a href="${lh}" target="_blank">${l}</a>${(links[i].rel.indexOf('nofollow') + 1) ? '&nbsp;&nbsp; — &nbsp;&nbsp;<b style="text-decoration:underline;">nofollow</b>' : ''}</li>`;
            } else if (lh.substr(0, 4) === 'http') {
                externalLinksCnt += 1;
                linksTempArr[lh] = true;
                externalLinks += `<li><a href="${lh}" target="_blank">${l}</a>${(links[i].rel.indexOf('nofollow') + 1) ? '&nbsp;&nbsp; — &nbsp;&nbsp;<b style="text-decoration:underline;">nofollow</b>' : ''}</li>`;
            }
        }

        for (let i = 0; i < img.length; i += 1) {
            if (img[i].alt && img[i].alt !== '') {
                altCnt += 1;
                altStrCnt += img[i].alt.length;
                altTitle += `<li><b>alt</b> — ${img[i].alt}</li>`;
            }
        }

        for (let i = 0; i < titleAttr.length; i += 1) {
            if (titleAttr[i].title && titleAttr[i].title !== '') {
                altCnt += 1;
                altStrCnt += titleAttr[i].title.length;
                altTitle += `<li><b>title</b> — ${titleAttr[i].title}</li>`;
            }
        }
        alertStr += `<p><b>alt + title:</b> <span title="Объем атрибутов a[alt] title (символов)">${altStrCnt}</span></p>`;
        alertStr += `<p><b>Объем текста:</b> <span title="Символов без пробелов">${bodyText.replace(/\s/gi, '').length}</span> | <span title="Символов с пробелами">${bodyText.length}</span></p>`;
        alertStr += `<p><b id="bbbb"></b></p>`;

        /* metrika */


        var ycounter = undefined;

        var myRegexp = /ym\((.+?), \"init\"/;
        for (var i = 0; i < document.scripts.length; i++) {
            var match = myRegexp.exec(document.scripts[i].text);
            if (match) {
                ycounter = match[1];
                break;
            }
        }
        console.log(ycounter);
        let  con = [];
        let  metr
        let y = 4414744;
        fetch(
            'https://api-metrika.yandex.net/management/v1/counter/'+ycounter, {
                headers: {
                    "Authorization": "OAuth AQAAAAAFlkdMAAb9J7SQkyZwfkENpT6zpDZ4WgA"
                }
            })
            .then(r => r.json())
            .then(metrikaApiJSON => {

                con = metrikaApiJSON;

                if(con['counter'] == undefined){
                    console.log('Нашей метрики нет');

                } else {
                    console.log('Метрика принадлежит:', con['counter']['owner_login']);
                    metr = 'Метрика: vova.skobrekov';


                    var el = document.getElementById('bbbb');
                    if (typeof el.innerText !== 'undefined') {
                        el.innerText = metr;
                    } else {
                        el.textContent = metr;
                    }
                }

            });



        /* end metrika*/







        for (let i = 0; i < hd.length; i += 1) {
            h16Str += `<li style="margin-left:${(hd[i].head - 1) * 20}px"${(hd[i].error) ? ` class="red" title="${hd[i].error}"` : ''}><span>H${hd[i].head} - ${hd[i].text}</span></li>`;
        }

        openLinks += '<nav style="display: grid !important;">';
        openLinks += `<b class="openLinksB" data="pxexternallinks">Внешние ссылки (${externalLinksCnt})</b>`;
        openLinks += `<b class="openLinksB" data="pxinternallinks">Внутренние ссылки (${internalLinksCnt})</b>`;
        openLinks += `<b class="openLinksB" data="pxalttitlelinks">Картинки (${altCnt})</b>`;
        openLinks += `<b class="openLinksB" data="pxh16links">H1-H6 ${(hErr) ? `<span class="red">(${hd.length})` : `(${hd.length})`}</b>`;
        openLinks += '<b class="openLinksB" data="pxtext">Текст</b>';
        openLinks += '<b class="openLinksB" data="pxtext2">Мета-теги</b>';
        openLinks += '<b class="openLinksB" data="pxtext3">Canonical</b>';
        openLinks += `<b class="openLinksB" data="googlespeed"><a  href="${spd}" target="_blank">Проверка скорости </a></b>`;
        openLinks += `<b class="openLinksB"><a  href="${screen}" target="_blank">Скриншот </a></b>`;
        openLinks += '<a class="openLinksB"  onclick="metatag()"><b>Выделить теги цветом</b></a>';
        openLinks += '</nav>';
        alertStr += `</div>`;




        const topBS = document.createElement('style');
        topBS.setAttribute('type', 'text/css');
        topBS.innerHTML = '.d_off{display:none !important;}.hide {left: -119px !important;}.pixelTopBlockWrp{position:relative;width:100%;top:0;left:0;background:#f8f8f8;z-index:999999;text-align:left;border-bottom:1px solid #9D9DA1;color:#000;font-family:arial;max-height:50%;overflow-y:auto;}.pixelTopBlockWrp .close {float:right;cursor:pointer;color:#000;font-size: 24px;line-height: 0;padding: 8px 0 0;}.topBlock {padding: 5px 10px !important;font-size: 14px !important;line-height: 16px !important;height: 100% !important;min-height: 590px !important;} .alertStr_block_info .title{text-align: center;width: 100%;float: right;} .topBlock p {margin: 0 0 0.3em 0 !important;padding: 0px;line-height: 1.2em;font-size: 11px;word-break: break-word;} .alertStr_block_info {background-color: #e1e1e1;padding: 9px;} .pxtblocklinks OL {margin: 5px 5px;padding: 0 0 0 40px;list-style: decimal;background-color: #f8f8f8;display:none;}.pxtblocklinks OL LI {color: #000;margin-bottom: 3px;display:block;font-size:14px;}.pxtblocklinks {width: 600px;position: absolute;background: #fff;z-index: 99999;box-shadow: 0 3px 10px #000;font-size: 14px;word-wrap: break-word;float: right;right: 0;left: 100%;margin: 0 auto;top: 0%;}.pxexternallinks, .pxinternallinks, .pxalttitlelinks, .pxh16links{margin:0px 15px;padding: 0 0 0 20px;list-style:decimal;display:none;height:500px;overflow:auto;} .pxh16links span:hover {cursor:pointer;border-bottom:1px solid;}.openLinksB {background-color: #e1e1e1;padding: 6px;margin-top: 7px;} .openLinksB:hover {border-bottom: none;} .pixelTopBlockWrp b, .pixelTopBlockWrp p{color:#000;} .pxtblocklinks a{color:#000;text-decoration:none;} .pxtblocklinks a:hover{border-bottom:1px solid;}.pixelTopBlockWrp b{font-weight:bold;}.topBlock .red, .pxtblocklinks .red {color:red;} .link_sim{text-decoration:underline;} .link_sim:hover{cursor:pointer; text-decoration:none;color:blue;} .topBlock a {text-transform: unset !important;color: black !important;text-align: left !important;line-height: 100% !important;} .pxtext{margin:0px 15px;padding: 0 20px 0 20px !important;display:none;height:500px;overflow:auto;} .pxtext2{margin:0px 15px;padding: 0 20px 0 20px !important;display:none;height:500px;overflow:auto;} .pxtext4{margin:0px 15px;padding: 0 20px 0 20px !important;display:none;height:500px;overflow:auto;} .pxtext3{margin:0px 15px;padding: 0 20px 0 20px !important;display:none;height:500px;overflow:auto;} .pxtext a {color:#0000ee;}';
        document.getElementsByTagName('body')[0].appendChild(topBS);
        const topBlock = document.createElement('div');
        topBlock.className = 'pixelTopBlockWrp';
        topBlock.innerHTML = `<div class="left_btn" style="background-color: #f8f8f8;
    position: fixed;
    margin-left: 119px;
    height: 100px;
    top: 41%;
    border: solid 2px #e1e1e1;
    border-radius: 0px 11px 11px 0px;
   
    text-orientation: revert;
    writing-mode: vertical-rl;
    padding: -1px;
    text-orientation: mixed;
    text-align: center;
    font-variant: all-small-caps;">toolbox</div><div class="topBlock"><b class="close">\u00d7</b>${alertStr}${openLinks}</div>`;
        const first = document.getElementsByTagName('body')[0].childNodes[0];

        const linksData = document.createElement('div');
        linksData.className = 'pxtblocklinks d_off';
        linksData.innerHTML = `<ol class="pxexternallinks">${externalLinks}</ol><ol class="pxinternallinks">${internalLinks}</ol><ol class="pxalttitlelinks">${altTitle}</ol><ol class="pxh16links" style="list-style:none;">${h16Str}</ol><ol class="pxtext">${codeScriptsDel.innerHTML}</ol><ol class="pxtext2"><br><b class="link_sim" title="Скопировать title в буфер обмена">Title</b> <span ${(title.textContent.length < 30 || title.textContent.length > 150) ? "class='red'" : ''}>(${title.textContent.length}): </span>${title.textContent}<br> <b class="link_sim" title="Скопировать description в буфер обмена">Description</b> <span ${(descr.length < 50 || descr.length > 250) ? "class='red'" : ''}>(${descr.length}): </span> ${descr}<br><b class="link_sim" title="Скопировать keywords в буфер обмена">Keywords</b> <span>(${keyw.length}): </span>${keyw}</ol><ol class="pxtext3"><p><b>Canonical:</b> ${(canonical === location.href) ? `<a href='${canonical}'>${canonical}</a>` : `<a href='${canonical}'><b class='red'>${canonical}</b></a>`}</p></ol>`;
        const block = document.createElement('div');
        block.style = 'position:fixed;z-index:9999999999999;width:120px;top:15%;left:0px;';
        block.className = 'pxtblock pxtagblock hide';
        block.appendChild(topBlock);
        block.appendChild(linksData);
        document.getElementsByTagName('body')[0].insertBefore(block, first);

        document.querySelector('div.pxtagblock b.close').onclick = () => {
            const pxtblock = document.querySelector('div.pxtagblock');
            document.getElementsByTagName('body')[0].removeChild(pxtblock);
        };

        const copyLink = document.querySelectorAll('b.link_sim');
        for (let i = 0; i < copyLink.length; i += 1) {
            copyLink[i].onclick = (e) => {
                const ta = document.createElement('textarea');
                const body = document.querySelector('body');
                body.appendChild(ta);
                ta.innerHTML = e.target.parentNode.lastChild.nodeValue;
                ta.select();
                document.execCommand('copy');
                body.removeChild(ta);
            };
        }
        const container = document.querySelector(".pxtblock");
        const info_block = document.querySelector(".pxtblocklinks");
        const btn_block = document.querySelector(".left_btn");
        btn_block.onclick = function(){
            container.classList.toggle("hide");
            info_block.classList.toggle("d_off");


            pxtblocklinks
        }

        const openLinksBlocks = document.querySelectorAll('div.pxtagblock b.openLinksB');
        const pxtblocklinks = document.querySelectorAll('div.pxtblocklinks ol');
        for (let i = 0; i < openLinksBlocks.length; i += 1) {
            openLinksBlocks[i].onclick = (e) => {
                for (let k = 0; k < pxtblocklinks.length; k += 1) {
                    const currentBlock = pxtblocklinks[k];
                    if (e.target.getAttribute('data') !== currentBlock.className) {
                        currentBlock.classList.remove('active');
                        currentBlock.style.display = 'none';
                    } else {
                        if (currentBlock.classList.contains('active')) {
                            currentBlock.classList.remove('active');
                            currentBlock.style.display = 'none';
                        } else {
                            currentBlock.classList.add('active');
                            currentBlock.style.display = 'block';
                        }
                    }
                }
            };
        }
    };
})();



